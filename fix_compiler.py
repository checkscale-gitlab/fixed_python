from pathlib import Path
import sysconfig
import configparser

stdlibDir = Path(sysconfig.get_paths()['platstdlib']).absolute()
distutilsCfg = stdlibDir / "distutils" / "distutils.cfg"

if __name__ == "__main__":
    c = configparser.ConfigParser()
    if distutilsCfg.exists():
        with distutilsCfg.open("rt", encoding="utf-8") as f:
            c.read(f)
    for sn in ("build", "build_ext"):
        if sn not in c:
            c[sn] = {}
        s = c[sn]
        s["compiler"] = "unix"
    with distutilsCfg.open("w", encoding="utf-8") as f:
        c.write(f)
